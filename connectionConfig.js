const fileStream = require('fs');
if (process.env.MODE === 'TEST') {
  connectionFilePath = './__mocks__/connection.json';
} else {
  connectionFilePath = './connection.json';
}
const connectionInfoObj = JSON.parse(fileStream.readFileSync(connectionFilePath, 'UTF-8'));
exports.storageConfig = connectionInfoObj;
