const validateTest = require('../../validate');

describe('validation functionality tests', () => {
  test('correct: Aggregation event', () => {
    const epcisDoc = '{\n' +
      '    "@context": [\n' +
      '        "https://gs1.github.io/EPCIS/epcis-context.jsonld",\n' +
      '        {\n' +
      '            "example": "https://example.org/epcis"\n' +
      '        }\n' +
      '    ],\n' +
      '    "eventID": "ni:///sha-256;87b5f18a69993f0052046d4687dfacdf48f7c988cfabda2819688c86b4066a49?ver=CBV2.0",\n' +
      '    "type": "AggregationEvent",\n' +
      '    "eventTime": "2013-06-08T14:58:56.591Z",\n' +
      '    "eventTimeZoneOffset": "+02:00",\n' +
      '    "parentID":"urn:epc:id:sscc:0614141.1234567890",\n' +
      '    "childEPCs":["urn:epc:id:sgtin:0614141.107346.2017","urn:epc:id:sgtin:0614141.107346.2018"],\n' +
      '    "action": "OBSERVE",\n' +
      '    "bizStep": "receiving",\n' +
      '    "disposition": "in_progress",\n' +
      '    "readPoint": {"id": "urn:epc:id:sgln:0614141.00777.0"},\n' +
      '    "bizLocation": {"id": "urn:epc:id:sgln:0614141.00888.0"},\n' +
      '    \n' +
      '    "childQuantityList": [\n' +
      '        {"epcClass":"urn:epc:idpat:sgtin:4012345.098765.*","quantity":10},\n' +
      '        {"epcClass":"urn:epc:class:lgtin:4012345.012345.998877","quantity":200.5,"uom":"KGM"}\n' +
      '    ],\n' +
      '    "example:myField":"Example of a vendor/user extension"\n' +
      '}';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).toBe('valid');
  });

  test('correct: Object event', () => {
    const epcisDoc = '{\n' +
      '    "@context": "https://gs1.github.io/EPCIS/epcis-context.jsonld",\n' +
      '    "eventID": "ni:///sha-256;df7bb3c352fef055578554f09f5e2aa41782150ced7bd0b8af24dd3ccb30ba69?ver=CBV2.0",\n' +
      '    "type": "ObjectEvent",\n' +
      '    "action": "OBSERVE",\n' +
      '    "bizStep": "shipping",\n' +
      '    "disposition": "in_transit",\n' +
      '    "epcList": ["urn:epc:id:sgtin:0614141.107346.2017","urn:epc:id:sgtin:0614141.107346.2018"],\n' +
      '    "eventTime": "2005-04-03T20:33:31.116000-06:00",\n' +
      '    "eventTimeZoneOffset": "-06:00",\n' +
      '    "readPoint": {"id": "urn:epc:id:sgln:0614141.07346.1234"},\n' +
      '    "bizTransactionList": [  {"type": "po", "bizTransaction": "http://transaction.acme.com/po/12345678" }  ]\n' +
      '}';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).toBe('valid');
  });

  test('correct: Transformation event', () => {
    const epcisDoc = '{\n' +
      '    "@context": [\n' +
      '        "https://gs1.github.io/EPCIS/epcis-context.jsonld",\n' +
      '        {\n' +
      '            "example": "https://example.org/epcis"\n' +
      '        }\n' +
      '    ],\n' +
      '    "eventID": "ni:///sha-256;e65c3a997e77f34b58306da7a82ab0fc91c7820013287700f0b50345e5795b97?ver=CBV2.0",\n' +
      '    "type": "TransformationEvent",\n' +
      '    "eventTime": "2013-10-31T14:58:56.591Z",\n' +
      '    "eventTimeZoneOffset": "+02:00",\n' +
      '    "inputEPCList": ["urn:epc:id:sgtin:4012345.011122.25","urn:epc:id:sgtin:4000001.065432.99886655"],\n' +
      '    "inputQuantityList": [{"epcClass":"urn:epc:class:lgtin:4012345.011111.4444","quantity":10,"uom":"KGM"},\n' +
      '            {"epcClass":"urn:epc:class:lgtin:0614141.077777.987","quantity":30},\n' +
      '            {"epcClass":"urn:epc:idpat:sgtin:4012345.066666.*","quantity":220}\t\t  \t\t\n' +
      '    ],\n' +
      '    "outputEPCList": [\n' +
      '        "urn:epc:id:sgtin:4012345.077889.25",\n' +
      '        "urn:epc:id:sgtin:4012345.077889.26",\n' +
      '        "urn:epc:id:sgtin:4012345.077889.27",\n' +
      '        "urn:epc:id:sgtin:4012345.077889.28"\n' +
      '    ],\n' +
      '  \n' +
      '    "bizStep": "commissioning",\n' +
      '    "disposition": "in_progress",\n' +
      '    "readPoint": {"id": "urn:epc:id:sgln:4012345.00001.0"},\n' +
      '    "ilmd": {"example:bestBeforeDate":"2014-12-10","example:batch":"XYZ" },\n' +
      '    "example:myField":"Example of a vendor/user extension"\n' +
      '}';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).toBe('valid');
  });

  test('correct: Association event', () => {
    const epcisDoc = '{\n' +
      '    "@context": "https://gs1.github.io/EPCIS/epcis-context.jsonld",\n' +
      '    "eventID": "ni:///sha-256;025ac144187a8c5e14caf4d1cfa69250a33dc59a5bc42a68d31b1b5e55a3f15a?ver=CBV2.0",\n' +
      '    "type": "AssociationEvent",\n' +
      '    "eventTime": "2019-11-01T14:00:00.000+01:00",\n' +
      '    "eventTimeZoneOffset": "+01:00",\n' +
      '    "parentID":"urn:epc:id:grai:4012345.55555.987",\n' +
      '    "childEPCs":["urn:epc:id:giai:4000001.12345"],\n' +
      '    "action": "ADD",\n' +
      '    "bizStep": "assembling",\n' +
      '    "readPoint": {"id": "urn:epc:id:sgln:4012345.00001.0"}\n' +
      '}';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).toBe('valid');
  });

  test('incorrect: missing @context', () => {
    const epcisDoc = '{\n' +
      '\t\t  ' +
      '"eventID": "ni:///sha-256;df7bb3c352fef055578554f09f5e2aa41782150ced7bd0b8af24dd3ccb30ba69?ver=CBV2.0",\n' +
      '\t\t  "type": "ObjectEvent",\n' +
      '\t\t  "action": "OBSERVE",\n' +
      '\t\t  "bizStep": "shipping",\n' +
      '\t\t  "disposition": "in_transit",\n' +
      '\t\t  "epcList": ["urn:epc:id:sgtin:0614141.107346.2017","urn:epc:id:sgtin:0614141.107346.2018"],\n' +
      '\t\t  "eventTime": "2013-06-08T14:58:56.591Z",\n' +
      '\t\t  "eventTimeZoneOffset": "+02:00",\n' +
      '\t\t  "readPoint": {"id": "urn:epc:id:sgln:0614141.07346.1234"},\n' +
      '\t\t  "bizTransactionList": [  {"type": "po", "bizTransaction": "http://transaction.acme.com/po/12345678" }' +
      '  ]\n' + '     }\n';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).not.toBe('valid');
  });

  test('incorrect: improper type', () => {
    const epcisDoc = '{\n' +
      '    "@context": "https://gs1.github.io/EPCIS/epcis-context.jsonld",\n' +
      '\t\t  ' +
      '"eventID": "ni:///sha-256;df7bb3c352fef055578554f09f5e2aa41782150ced7bd0b8af24dd3ccb30ba69?ver=CBV2.0",\n' +
      '\t\t  "type": "StupidEvent",\n' +
      '\t\t  "action": "YOU\'RE_STUPID",\n' +
      '\t\t  "bizStep": "shipping",\n' +
      '\t\t  "disposition": "in_transit",\n' +
      '\t\t  "epcList": ["urn:epc:id:sgtin:0614141.107346.2017","urn:epc:id:sgtin:0614141.107346.2018"],\n' +
      '\t\t  "eventTime": "sometime",\n' +
      '\t\t  "eventTimeZoneOffset": "so dumb",\n' +
      '\t\t  "readPoint": {"id": "urn:epc:id:sgln:0614141.07346.1234"},\n' +
      '\t\t  "bizTransactionList": [  {"type": "po", "bizTransaction": "http://transaction.acme.com/po/12345678" }  ' +
      ']\n' + '     }\n';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).not.toBe('valid');
  });

  test('incorrect: eventTime improper formatting', () => {
    const epcisDoc = '{\n' +
      '    "@context": [\n' +
      '        "https://gs1.github.io/EPCIS/epcis-context.jsonld",\n' +
      '        {\n' +
      '            "example": "https://example.org/epcis"\n' +
      '        }\n' +
      '    ],\n' +
      '    "eventID": "ni:///sha-256;87b5f18a69993f0052046d4687dfacdf48f7c988cfabda2819688c86b4066a49?ver=CBV2.0",\n' +
      '    "type": "AggregationEvent",\n' +
      '    "eventTime": "NO_TIME_FOR_FUN",\n' +
      '    "eventTimeZoneOffset": "+02:00",\n' +
      '    "parentID":"urn:epc:id:sscc:0614141.1234567890",\n' +
      '    "childEPCs":["urn:epc:id:sgtin:0614141.107346.2017","urn:epc:id:sgtin:0614141.107346.2018"],\n' +
      '    "action": "OBSERVE",\n' +
      '    "bizStep": "receiving",\n' +
      '    "disposition": "in_progress",\n' +
      '    "readPoint": {"id": "urn:epc:id:sgln:0614141.00777.0"},\n' +
      '    "bizLocation": {"id": "urn:epc:id:sgln:0614141.00888.0"},\n' +
      '    \n' +
      '    "childQuantityList": [\n' +
      '        {"epcClass":"urn:epc:idpat:sgtin:4012345.098765.*","quantity":10},\n' +
      '        {"epcClass":"urn:epc:class:lgtin:4012345.012345.998877","quantity":200.5,"uom":"KGM"}\n' +
      '    ],\n' +
      '    "example:myField":"Example of a vendor/user extension"\n' +
      '}';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).not.toBe('valid');
  });

  test('incorrect: improper bizStep', () => {
    const epcisDoc = '{\n' +
      '    "@context": [\n' +
      '        "https://gs1.github.io/EPCIS/epcis-context.jsonld",\n' +
      '        {\n' +
      '            "example": "https://example.org/epcis"\n' +
      '        }\n' +
      '    ],\n' +
      '    "eventID": "ni:///sha-256;e65c3a997e77f34b58306da7a82ab0fc91c7820013287700f0b50345e5795b97?ver=CBV2.0",\n' +
      '    "type": "TransformationEvent",\n' +
      '    "eventTime": "2013-10-31T14:58:56.591Z",\n' +
      '    "eventTimeZoneOffset": "+02:00",\n' +
      '    "inputEPCList": ["urn:epc:id:sgtin:4012345.011122.25","urn:epc:id:sgtin:4000001.065432.99886655"],\n' +
      '    "inputQuantityList": [{"epcClass":"urn:epc:class:lgtin:4012345.011111.4444","quantity":10,"uom":"KGM"},\n' +
      '            {"epcClass":"urn:epc:class:lgtin:0614141.077777.987","quantity":30},\n' +
      '            {"epcClass":"urn:epc:idpat:sgtin:4012345.066666.*","quantity":220}\t\t  \t\t\n' +
      '    ],\n' +
      '    "outputEPCList": [\n' +
      '        "urn:epc:id:sgtin:4012345.077889.25",\n' +
      '        "urn:epc:id:sgtin:4012345.077889.26",\n' +
      '        "urn:epc:id:sgtin:4012345.077889.27",\n' +
      '        "urn:epc:id:sgtin:4012345.077889.28"\n' +
      '    ],\n' +
      '  \n' +
      '    "bizStep": "nah",\n' +
      '    "disposition": "in_progress",\n' +
      '    "readPoint": {"id": "urn:epc:id:sgln:4012345.00001.0"},\n' +
      '    "ilmd": {"example:bestBeforeDate":"2014-12-10","example:batch":"XYZ" },\n' +
      '    "example:myField":"Example of a vendor/user extension"\n' +
      '}';
    const parsed = JSON.parse(epcisDoc);
    expect(validateTest.validateEpcis(parsed)).not.toBe('valid');
  });
});
