const tangle = require('../../tangle.js');
jest.setTimeout(1000000);

test('test tangle send', () => {
  return tangle.send('API_Test', 'This is a unit test')
      .then((result) => {
        expect(result).toBeDefined();
      });
});

test('test tangle retrieve', () => {
  return tangle.retrieveByMessageIndex('API_Test')
      .then((result) => {
        expect(result).toBeDefined();
      });
});
