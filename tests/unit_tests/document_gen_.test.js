const generator = require('../../document_generator.js');

test('Test document generator mutiple events', () => {
  const events = [
    {
      '@context': 'https://id.gs1.org/epcis-context.jsonld',
      'type': 'AssociationEvent',
      'eventTime': '2019-11-03T14:00:00+01:00',
      'eventTimeZoneOffset': '+01:00',
      'parentID': 'urn:epc:id:grai:4012345.55555.987',
      'eventID': 'ni:///sha-256;902e54bb7c203b43c80c19495ed1c96bd89bae43e31c1e91eb9cc3b3dd65e1cc?ver=CBV2.0',
      'childEPCs': [
        'urn:epc:id:giai:4000001.12345',
      ],
      'action': 'DELETE',
      'bizStep': 'removing',
      'readPoint': {
        'id': 'urn:epc:id:sgln:4012345.00002.0',
      },
      'inputEPCList': [
        'urn:epc:id:sgtin:4012345.011122.25',
        'urn:epc:id:sgtin:4000001.065432.99886655',
      ],
      'outputEPCList': [
        'urn:epc:id:sgtin:4012345.077889.25',
        'urn:epc:id:sgtin:4012345.077889.26',
        'urn:epc:id:sgtin:4012345.077889.27',
        'urn:epc:id:sgtin:4012345.077889.28',
      ],
    },

    {
      '@context': 'https://id.gs1.org/epcis-context.jsonld',
      'type': 'AssociationEvent',
      'eventTime': '2019-11-03T14:00:00+01:00',
      'eventTimeZoneOffset': '+01:00',
      'parentID': 'urn:epc:id:grai:4012345.55555.987',
      'eventID': 'ni:///sha-256;902e54bb7c203b43c80c19495ed1c96bd89bae43e31c1e91eb9cc3b3dd65e1cc?ver=CBV2.0',
      'childEPCs': [
        'urn:epc:id:giai:4000001.12345',
      ],
      'action': 'DELETE',
      'bizStep': 'removing',
      'readPoint': {
        'id': 'urn:epc:id:sgln:4012345.00002.0',
      },
      'inputEPCList': [
        'urn:epc:id:sgtin:4012345.011122.25',
        'urn:epc:id:sgtin:4000001.065432.99886655',
      ],
      'outputEPCList': [
        'urn:epc:id:sgtin:4012345.077889.25',
        'urn:epc:id:sgtin:4012345.077889.26',
        'urn:epc:id:sgtin:4012345.077889.27',
        'urn:epc:id:sgtin:4012345.077889.28',
      ],
    },

    {
      '@context': 'https://id.gs1.org/epcis-context.jsonld',
      'type': 'AssociationEvent',
      'eventTime': '2019-11-03T14:00:00+01:00',
      'eventTimeZoneOffset': '+01:00',
      'parentID': 'urn:epc:id:grai:4012345.55555.987',
      'eventID': 'ni:///sha-256;902e54bb7c203b43c80c19495ed1c96bd89bae43e31c1e91eb9cc3b3dd65e1cc?ver=CBV2.0',
      'childEPCs': [
        'urn:epc:id:giai:4000001.12345',
      ],
      'action': 'DELETE',
      'bizStep': 'removing',
      'readPoint': {
        'id': 'urn:epc:id:sgln:4012345.00002.0',
      },
      'inputEPCList': [
        'urn:epc:id:sgtin:4012345.011122.25',
        'urn:epc:id:sgtin:4000001.065432.99886655',
      ],
      'outputEPCList': [
        'urn:epc:id:sgtin:4012345.077889.25',
        'urn:epc:id:sgtin:4012345.077889.26',
        'urn:epc:id:sgtin:4012345.077889.27',
        'urn:epc:id:sgtin:4012345.077889.28',
      ],
    },

  ];

  let count = 0;
  const array = [];

  for (key in events) {
    if (events.hasOwnProperty(key)) {
      array.push(JSON.stringify(events[key]));
    }
  }

  const doc = generator.generateDoc(array);
  for (key in doc['epcisBody']['eventList']) {
    if (doc['epcisBody']['eventList'].hasOwnProperty(key)) {
      count++;
    }
  }

  console.log(count);
  expect(count).toEqual(array.length);
});

test('Test document generator single event', () => {
  const events = [
    {
      '@context': 'https://id.gs1.org/epcis-context.jsonld',
      'type': 'AssociationEvent',
      'eventTime': '2019-11-03T14:00:00+01:00',
      'eventTimeZoneOffset': '+01:00',
      'parentID': 'urn:epc:id:grai:4012345.55555.987',
      'eventID': 'ni:///sha-256;902e54bb7c203b43c80c19495ed1c96bd89bae43e31c1e91eb9cc3b3dd65e1cc?ver=CBV2.0',
      'childEPCs': [
        'urn:epc:id:giai:4000001.12345',
      ],
      'action': 'DELETE',
      'bizStep': 'removing',
      'readPoint': {
        'id': 'urn:epc:id:sgln:4012345.00002.0',
      },
      'inputEPCList': [
        'urn:epc:id:sgtin:4012345.011122.25',
        'urn:epc:id:sgtin:4000001.065432.99886655',
      ],
      'outputEPCList': [
        'urn:epc:id:sgtin:4012345.077889.25',
        'urn:epc:id:sgtin:4012345.077889.26',
        'urn:epc:id:sgtin:4012345.077889.27',
        'urn:epc:id:sgtin:4012345.077889.28',
      ],
    },

  ];

  let count = 0;
  const array = [];

  for (key in events) {
    if (events.hasOwnProperty(key)) {
      array.push(JSON.stringify(events[key]));
    }
  }

  const doc = generator.generateDoc(array);

  for (key in doc['epcisBody']['eventList']) {
    if (doc['epcisBody']['eventList'].hasOwnProperty(key)) {
      count++;
    }
  }

  console.log(count);
  expect(count).toEqual(array.length);
});


test('Test document generator single event', () => {
  const events = [];
  let count = 0;
  const array = [];

  for (key in events) {
    if (events.hasOwnProperty(key)) {
      array.push(JSON.stringify(events[key]));
    }
  }

  const doc = generator.generateDoc(array);

  for (key in doc['epcisBody']['eventList']) {
    if (doc['epcisBody']['eventList'].hasOwnProperty(key)) {
      count++;
    }
  }
  console.log(count);
  expect(count).toEqual(array.length);
});
