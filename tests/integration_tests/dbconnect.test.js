/*
  integration tests with docker postgres db
  requires docker and postgresql-common (for pg_isready) to run locally
*/
process.env.MODE = 'TEST';
const db = require('../../db-connect');

const testEpcis = '{ "@context": "https://id.gs1.org/epcis-context.jsonld", ' +
  '"type": "AssociationEvent", ' +
  '"eventTime": "2019-11-03T14:00:00+01:00", "eventTimeZoneOffset": "+01:00", ' +
  '"parentID": "urn:epc:id:grai:4012345.55555.987", ' +
  '"eventID": "ni:///sha-256;902e54bb7c203b43c80c19495ed1c96bd89bae43e31c1e91eb9cc3b3dd65e1cc?ver=CBV2.0", ' +
  '"childEPCs": [ "urn:epc:id:giai:4000001.12345" ], "action": "DELETE", "bizStep": "removing", ' +
  '"readPoint": { "id": "urn:epc:id:sgln:4012345.00002.0" }, ' +
  '"inputEPCList": [ "urn:epc:id:sgtin:4012345.011122.25", ' +
  '"urn:epc:id:sgtin:4000001.065432.99886655" ], "outputEPCList": [ "urn:epc:id:sgtin:4012345.077889.25", ' +
  '"urn:epc:id:sgtin:4012345.077889.26" ] }';
const testParsed = JSON.parse(testEpcis);
const testMId = '8e26806e70a0f9a2bfceaaafcdd20b1aabdaff026a47fb3b2226181f0518a605';
const testMIndex = '902e54bb7c203b43c80c19495ed1c96bd89bae43e31c1e91eb9cc3b3dd65e1cc';
jest.setTimeout(1000000);

afterEach(async () => {
  await db.wipeDB();
});

describe('db integration tests: insert and select back', () => {
  test('insert one event', () => {
    return db.insertDb(testParsed, testMIndex, testMId)
        .then( () => {
          db.getEventTypeEvent('AssociationEvent', testMIndex)
              .then( (result) => {
                expect(result.rows.sort()).toContainEqual([testMId, testMIndex].sort());
              });
        });
  });

  test('insert multiple epcs into epc table', () => {
    return db.insertDb(testParsed, testMIndex, testMId)
        .then( () => {
          db.insertDbEpc(testParsed, testMIndex, testMId)
              .then(() => {
                db.getEpcs()
                    .then((result) => {
                      const expected = [[testParsed['parentID']], [testParsed['childEPCs'][0]],
                        [testParsed['inputEPCList'][0]], [testParsed['inputEPCList'][1]],
                        [testParsed['outputEPCList'][0]], [testParsed['outputEPCList'][1]]];
                      expect(result.rows.sort()).toEqual(expected.sort());
                    });
              });
        });
  });
});


