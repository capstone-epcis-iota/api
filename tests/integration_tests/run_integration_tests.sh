#!/bin/bash

# build the database container, we want to rebuild each time to pull a new remote schema (or use the cache)
docker build ./tests/integration_tests/ -t postgres:capstone
# run the container in detached mode, bind to localhost, scripts in the container bootstrap the db
docker run --name iota_db --rm -d -p 5432:5432/tcp postgres:capstone

# hackery shit to loop until the db is ready
while [[ $i != 0 ]]
do
  # tests the db to see if it's ready
  pg_isready -d postgres -h localhost -p 5432 -q
  # check the return code
  i=$?
  sleep 1
done

# exec into the container to bootstrap the local db with the schema of the remote
echo "Bootstrapping Docker db with schema of remote db..."
docker exec iota_db psql -U postgres -d postgres -f /tmp/iota_db.sql
# run the tests
jest --ci --reporters=default --reporters=jest-junit --testPathPattern=./tests/integration_tests
# stop the container, it cleans itself up when it stops
echo "Stopping container..."
docker container stop iota_db