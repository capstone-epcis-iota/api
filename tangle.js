const {ClientBuilder} = require('@iota/client');

// / send message to the tangle with specified index and data(epcis event)
exports.send = async function(index, data) {
  // client will connect to testnet by default, rebuilding to avoid pooling issues
  const client = new ClientBuilder().build();
  // check for working node info
  client.getInfo().then((someJSON) => {
    const nodeJSON = JSON.stringify(someJSON);
    const fs = require('fs');
    fs.writeFile('log.txt', nodeJSON, (err) => {
      // In case of a print error
      if (err) console.log(err);
    });
  }).catch((err) => {
    console.log(err);
  });
  try {
    const message = await client.message()
        .index(index)
        .data(data)
        .submit();
    console.log(message);
    console.log(message.messageId);
    console.log(data);
    return message.messageId.toString();
  } catch (err) {
    console.log(err);
    throw Error('Not able to send message to tangle');
  }
};

// / retrieves messages from the iota ledger
exports.retrieveByMessageIndex = async function(messageIndex) {
  console.log(`${messageIndex}`);
  const client = new ClientBuilder().build();

  // get indexation data by index
  const messageIds = await client.getMessage().index(messageIndex);
  // const messageId = messageIds.find((id) => id == mId.toString());
  const messageWrapper = await client.getMessage().data(messageIds[0]);
  const data = Buffer.from(messageWrapper.message.payload.data, 'hex').toString('utf8');
  if (data != null) {
    return data;
  } else {
    throw Error('Could not retrieve message');
  }
};

//  same as reuglar retrieve byt retrieved by message ID instead of index
exports.retrieveByMessageId = async function(mId) {
  console.log(`Message Id: ${mId}`);
  const client = new ClientBuilder().build();
  //  fetch message from iota ledger
  const messageWrapper = await client.getMessage().data(mId);
  const data = Buffer.from(messageWrapper.message.payload.data, 'hex').toString('utf8');
  if (data != null) {
    return data;
  } else {
    throw Error('Could not retrieve message');
  }
};

