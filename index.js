const express = require('express');
const app = express();
app.use(express.json());
const tangle = require('./tangle.js');
const validate = require('./validate.js');
const dbConnect = require('./db-connect.js');
const generator = require('./document_generator.js');
const process = require('./process.js');
const serverName = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com';

// Default values for Cursor-based pagination
const MAX_INT = 2147483647;
const DEFAULT_LIMIT = 5;
const NEXT = ['<', 'DESC'];
const PREV = ['>=', 'ASC'];

app.get('/', (req, res) => {
  // roll for initiative, friend
  try {
    res.sendFile(__dirname + '/welcome.jpg');
  } catch (e) {
    console.log(e.toString());
  }
});

// retrieves a single epcis event from the tangle
app.get('/messageIndex/:id', (req, res) => {
  // / async call to tangle
  const data = tangle.retrieveByMessageId(req.params.id);
  data.then((myJson) => {
    const genJSON = JSON.parse(myJson);
    res.status(202).send(genJSON);
  }).catch((err) => {
    res.status(400).send(err.toString());
  });
});

// / will put a valid epcis event into database and on the tangle
app.post('/capture', (req, res) => {
  // perform EPCIS document validation according to GS1 JSON Schema with type-checking
  const jsonErr = validate.validateEpcis(req.body);
  if (jsonErr === 'valid') {
    console.log('This EPCIS event is valid');
    const epcisPost = JSON.parse(JSON.stringify(req.body));
    let id = epcisPost.eventID.toString();
    //  pattern match the id to format correctly for database storage
    id = /[0-9,a-f]{64}/.exec(id)[0];
    const epcisData = JSON.stringify(req.body);
    // const messageId = tangle.send(id, epcisData);
    //  store into database and store on tangle

    tangle.send(id, epcisData).then((mId) => {
      dbConnect.insertDb(epcisPost, id, mId).then((success) => {
        dbConnect.insertDbEpc(epcisPost, id, mId).then((epcSuccess) => {
          console.log('full insert completed');
          res.status(202).send(epcisPost);
        }).catch((err) => {
          console.error(err.toString());
          res.status(500).send(err.toString());
        });
      }).catch((err) => {
        console.error(err.toString());
        res.status(500).send(err.toString());
      });
    }).catch((error) => {
      console.error(err.toString());
      res.status(500).send(err.toString());
    });
  } else {
    res.status(400).send('Invalid Epcis Event');
  }
});

// EPC endpoints

// / returns all epcs available
//  stores results from and SQL query into a JSON object
app.get('/epcs', (req, res) => {
  dbConnect.getEpcs().then((result) => {
    //  JSON Object for storing array epcs available
    const object = {
      'epcsList': [],
    };
    const myRows = result.rows;
    for (let i = 0; i < myRows.length; i++) {
      object['epcsList'].push(myRows[i][0]);
    }
    res.status(202).send(object);
  }).catch((err) => {
    res.status(500).send(err);
  });
});

app.get('/epcs/:epc', (req, res) => {
  res.redirect(308, '/epcs/' + req.params.epc + '/events');
});

//  returns all events with specified epc into an EPCIS document
app.get('/epcs/:epc/events', (req, res) => {
  //  pagination limts
  let limit = req.query.limit;
  let lastId = req.query.lastId;
  let direction = req.query.direction;
  let directionArg = 'next';
  if (limit == null || limit == undefined) {
    limit = DEFAULT_LIMIT;
  }
  if (lastId == null || lastId == undefined) {
    lastId = MAX_INT;
  }
  if (direction === 'prev') {
    direction = PREV;
    directionArg = 'prev';
  } else {
    direction = NEXT;
  }
  dbConnect.getEpcEvents(req.params.epc, limit, lastId, direction).then((result) => {
    const myRows = result.rows;
    // / empty query result
    if (myRows.length === 0) {
      res.status(400).send('no result for epc');
      return;
    }
    //  generate a paginated epcis document
    process.processRows(myRows).then((myEvents) => {
      const doc = generator.generateDoc(myEvents);
      let nextIndex = myRows[myRows.length-1][1];
      let prevIndex = lastId;
      if (directionArg === 'prev') {
        prevIndex = myRows[myRows.length-1][1]+1;
        nextIndex = myRows[0][1];
      }
      const object = {
        '_links': {
          'base': `${serverName}:4000/epcs/${req.params.epc}/events`,
          'self': `${serverName}:4000/epcs/${req.params.epc}/events?` +
            `limit=${limit}&lastId=${lastId}&direction=${directionArg}`,
        },
        'EPCIS': doc,
      };
      if (((limit == myRows.length && directionArg === 'prev') || directionArg === 'next') && lastId != MAX_INT) {
        object._links.prev = `${serverName}:4000/epcs/${req.params.epc}/events?` +
        `limit=${limit}&lastId=${prevIndex}&direction=prev`;
      }
      if (limit == myRows.length) {
        object._links.next = `${serverName}:4000/epcs/${req.params.epc}/events?` +
            `limit=${limit}&lastId=${nextIndex}&direction=next`;
      }
      res.status(202).send(object);
    }).catch((processorError) => {
      res.status(500).send('Tangle Record Was Removed');
    });
  }).catch((err) => {
    res.status(500).send(err);
  });
});

//  returns an epcis document with a single event with specified eventID and epc code
app.get('/epcs/:epc/events/:eventID', (req, res) => {
  //  call to database with specified query
  dbConnect.getEpcEvent(req.params.epc, req.params.eventID).then((result) => {
    const myRows = result.rows;
    if (myRows.length === 0) {//  no result for the EPC
      res.status(400).send('no result for epc or eventID');
      return;
    }
    //  retrieve the event from the tangle
    tangle.retrieveByMessageId(myRows[0][0]).then((tangleResult) => {
      //  generate an epcis document
      const doc = generator.singleEventDoc(tangleResult);
      res.status(202).send(doc);
    }).catch((tangleError) => {
      res.status(500).send(tangleError);
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

//  returns all available bizLocations wrapped in JSON with an array
app.get('/bizLocations', (req, res) => {
  //  query to SQL database
  dbConnect.getFieldList('biz_location').then((result) => {
    console.log(result);
    const object = {
      'bizLocations': [],
    };
    const myRows = result.rows;
    for (let i = 0; i < myRows.length; i++) {
      object['bizLocations'].push(myRows[i][0]);
    }
    res.status(202).send(object);
  }).catch((err) => {
    res.status(500).send(err);
  });
});

app.get('/bizLocations/:bizLocation', (req, res) => {
  res.redirect(308, '/bizLocations/' + req.params.bizLocation + '/events');
});

//  returns all epcis events with specific bizlocation in a epcis document
app.get('/bizLocations/:bizLocation/events', (req, res) => {
  //  Limits for pagination
  let limit = req.query.limit;
  let lastId = req.query.lastId;
  let direction = req.query.direction;
  let directionArg = 'next';
  if (limit == null || limit == undefined) {
    limit = DEFAULT_LIMIT;
  }
  if (lastId == null || lastId == undefined) {
    lastId = MAX_INT;
  }
  if (direction === 'prev') {
    direction = PREV;
    directionArg = 'prev';
  } else {
    direction = NEXT;
  }
  dbConnect.getFieldEvents(['biz_location', req.params.bizLocation], limit, lastId).then((result) => {
    const myRows = result.rows;
    //  empty query result
    if (myRows.length === 0) {
      res.status(400).send('no result for bizLocation');
      return;
    }
    //  generate paginated epcis document
    process.processRows(myRows).then((myEvents) => {
      const doc = generator.generateDoc(myEvents);
      let nextIndex = myRows[myRows.length-1][1];
      let prevIndex = lastId;
      if (directionArg === 'prev') {
        prevIndex = myRows[myRows.length-1][1]+1;
        nextIndex = myRows[0][1];
      }
      const object = {
        '_links': {
          'base': `${serverName}:4000/bizLocations/${req.params.bizLocation}/events`,
          'self': `${serverName}:4000/bizLocations/${req.params.bizLocation}/events?` +
            `limit=${limit}&lastId=${lastId}&direction${directionArg}`,
        },
        'EPCIS': doc,
      };
      if (((limit == myRows.length && directionArg === 'prev') || directionArg === 'next') && lastId != MAX_INT) {
        object._links.prev = `${serverName}:4000/bizLocations/${req.params.bizLocation}/events?` +
        `limit=${limit}&lastId=${prevIndex}&direction=prev`;
      }
      if (limit == myRows.length) {
        object._links.next = `${serverName}:4000/bizLocations/${req.params.bizLocation}/events?` +
            `limit=${limit}&lastId=${nextIndex}&direction=next`;
      }
      res.status(202).send(object);
    }).catch((processorError) => {
      res.status(500).send('Tangle Record Was Removed');
    });
  }).catch((err) => {
    res.status(500).send(err);
  });
});

// / returns all events with specific bizLocation and eventId
app.get('/bizLocations/:bizLocation/events/:eventID', (req, res) => {
  dbConnect.getFieldEvent(['biz_location', req.params.bizLocation], req.params.eventID).then((result) => {
    const myRows = result.rows;
    // / no result for query result
    if (myRows.length === 0) {
      res.status(406).send('not an applicable bizLocation or eventID');
      return;
    }
    tangle.retrieveByMessageId(myRows[0][0]).then((tangleResult) => {
      const doc = generator.singleEventDoc(tangleResult);
      res.status(202).send(doc);
    }).catch((tangleError) => {
      res.status(500).send(tangleError);
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

// BizStep endpoints

//  returns all available bizsteps wrapped in JSON array
app.get('/bizSteps', (req, res) => {
  dbConnect.getFieldList('biz_step').then((result) => {
    const object = {
      'bizSteps': [],
    };
    const myRows = result.rows;
    for (let i = 0; i < myRows.length; i++) {
      object['bizSteps'].push(myRows[i][0]);
    }
    res.status(202).send(object);
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

app.get('/bizSteps/:bizStep', (req, res) => {
  res.redirect(308, '/bizSteps/' + req.params.bizStep + '/events');
});

//  returns all epcis events with specific bizstep in a epcis doc
app.get('/bizSteps/:bizStep/events', (req, res) => {
  //  pagination limits
  let limit = req.query.limit;
  let lastId = req.query.lastId;
  let direction = req.query.direction;
  let directionArg = 'next';
  if (limit == null || limit == undefined) {
    limit = DEFAULT_LIMIT;
  }
  if (lastId == null || lastId == undefined) {
    lastId = MAX_INT;
  }
  if (direction === 'prev') {
    direction = PREV;
    directionArg = 'prev';
  } else {
    direction = NEXT;
  }
  dbConnect.getFieldEvents(['biz_step', req.params.bizStep.toString()], limit, lastId, direction).then((result) => {
    const myRows = result.rows;
    // / empty query result
    if (myRows.length === 0) {
      res.status(400).send('no result for bizStep');
      return;
    }
    //  generate paginated epcis doc
    process.processRows(myRows).then((myEvents) => {
      console.log(myEvents);
      const doc = generator.generateDoc(myEvents);
      let nextIndex = myRows[myRows.length-1][1];
      let prevIndex = lastId;
      if (directionArg === 'prev') {
        prevIndex = myRows[myRows.length-1][1]+1;
        nextIndex = myRows[0][1];
      }
      const object = {
        '_links': {
          'base': `${serverName}:4000/bizSteps/${req.params.bizStep}/events`,
          'self': `${serverName}:4000/bizSteps/${req.params.bizStep}/events?` +
            `limit=${limit}&lastId=${lastId}&direction${directionArg}`,
        },
        'EPCIS': doc,
      };
      if (((limit == myRows.length && directionArg === 'prev') || directionArg === 'next') && lastId != MAX_INT) {
        object._links.prev = `${serverName}:4000/bizSteps/${req.params.bizStep}/events?` +
        `limit=${limit}&lastId=${prevIndex}&direction=prev`;
      }
      if (limit == myRows.length) {
        object._links.next = `${serverName}:4000/bizSteps/${req.params.bizStep}/events?` +
            `limit=${limit}&lastId=${nextIndex}&direction=next`;
      }
      res.status(202).send(object);
    }).catch((processorError) => {
      res.status(500).send('Tangle Record Was Removed');
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

//  returns a epcis event with specific bizstep and eventID
app.get('/bizSteps/:bizStep/events/:eventID', (req, res) => {
  console.log(`${req.params.bizStep}, ${req.params.eventID}`);
  dbConnect.getFieldEvent(['biz_step', req.params.bizStep], req.params.eventID.toString()).then((result) => {
    const myRows = result.rows;
    // / empty database query result
    if (myRows.length === 0) {
      res.status(400).send('no result for bizStep or eventID');
      return;
    }
    tangle.retrieveByMessageId(myRows[0][0]).then((tangleResult) => {
      //  generate  epcis document
      const doc = generator.singleEventDoc(tangleResult);
      res.status(202).send(doc);
    }).catch((tangleError) => {
      res.status(500).send(tangleError);
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

// ReadPoint endpoints

//  returns all readpoints in the database
app.get('/readPoints', (req, res) => {
  dbConnect.getFieldEvent('read_point').then((result) => {
    const object = {
      'readPoints': [],
    };
    const myRows = result.rows;
    for (let i = 0; i < myRows.length; i++) {
      object['readPoints'].push(myRows[i][0]);
    }
    res.status(202).send(object);
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

// dont know if we are going to implement this
app.get('/readPoints/:readPoint', (req, res) => {
  res.redirect(308, '/readPoints/' + req.params.readPoint + '/events');
});


// returns all events with specific readpoint in a epcis document
app.get('/readPoints/:readPoint/events', (req, res) => {
  //  pagination limits
  let limit = req.query.limit;
  let lastId = req.query.lastId;
  let direction = req.query.direction;
  let directionArg = 'next';
  if (limit == null || limit == undefined) {
    limit = DEFAULT_LIMIT;
  }
  if (lastId == null || lastId == undefined) {
    lastId = MAX_INT;
  }
  if (direction === 'prev') {
    direction = PREV;
    directionArg = 'prev';
  } else {
    direction = NEXT;
  }
  dbConnect.getFieldEvents(['read_point', req.params.readPoint], limit, lastId, direction).then((result) => {
    const myRows = result.rows;
    // / empty database query
    if (myRows.length === 0) {
      res.status(400).send('no result for readPoint');
      return;
    }
    //  generate epcis document
    process.processRows(myRows).then((myEvents) => {
      const doc = generator.generateDoc(myEvents);
      let nextIndex = myRows[myRows.length-1][1];
      let prevIndex = lastId;
      if (directionArg === 'prev') {
        prevIndex = myRows[myRows.length-1][1]+1;
        nextIndex = myRows[0][1];
      }
      const object = {
        '_links': {
          'base': `${serverName}:4000/readPoints/${req.params.readPoint}/events`,
          'self': `${serverName}:4000/readPoints/${req.params.readPoint}/events?` +
            `limit=${limit}&lastId=${lastId}&direction=${directionArg}`,
        },
        'EPCIS': doc,
      };
      if (((limit == myRows.length && directionArg === 'prev') || directionArg === 'next') && lastId != MAX_INT) {
        object._links.prev = `${serverName}:4000/readPoints/${req.params.readPoint}/events?` +
        `limit=${limit}&lastId=${prevIndex}&direction=prev`;
      }
      if (limit == myRows.length) {
        object._links.next = `${serverName}:4000/readPoints/${req.params.readPoint}/events?` +
            `limit=${limit}&lastId=${nextIndex}&direction=next`;
      }
      res.status(202).send(object);
    }).catch((processorError) => {
      res.status(500).send('Tangle Record Removed');
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

//  returns epcis event with specific readpoint in a epcis doc
app.get('/readPoints/:readPoint/events/:eventID', (req,
    res) => {
  dbConnect.getFieldEvent(['read_point', req.params.readPoint],
      req.params.eventID.toString()).then((result) => {
    const myRows = result.rows;
    // / empty query result
    if (myRows.length === 0) {
      res.status(400).send('no result for readPoint or eventID');
      return;
    }
    tangle.retrieveByMessageId(myRows[0][0]).then((tangleResult) => {
      const doc = generator.singleEventDoc(tangleResult);
      res.status(202).send(doc);
    }).catch((tangleError) => {
      console.log(tangleError);
      res.status(500).send(tangleError);
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

// Disposition endpoints

// returns all available dispostions in the database in JSON with an array
app.get('/dispositions', (req, res) => {
  dbConnect.getFieldList('disposition').then((result) => {
    const object = {
      'dispositions': [],
    };
    const myRows = result.rows;
    for (let i = 0; i < myRows.length; i++) {
      object['dispositions'].push(myRows[i][0]);
    }
    res.status(202).send(object);
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

app.get('/dispositions/:disposition', (req, res) => {
  res.redirect(308, '/dispositions/' + req.params.disposition + '/events');
});

// returns all events for a specific dispostion in a epcis doc
app.get('/dispositions/:disposition/events', (req, res) => {
  //  pagination limits
  let limit = req.query.limit;
  let lastId = req.query.lastId;
  let direction = req.query.direction;
  let directionArg = 'next';
  if (limit == null || limit == undefined) {
    limit = DEFAULT_LIMIT;
  }
  if (lastId == null || lastId == undefined) {
    lastId = MAX_INT;
  }
  if (direction === 'prev') {
    direction = PREV;
    directionArg = 'prev';
  } else {
    direction = NEXT;
  }
  dbConnect.getFieldEvents(['disposition', req.params.disposition], limit, lastId, direction).then((result) => {
    console.log(result);
    const myRows = result.rows;
    // / No result for query
    if (myRows.length === 0) {
      res.status(400).send('no result for disposition');
      return;
    }
    process.processRows(myRows).then((myEvents) => {
      //  generate an epcis doc
      const doc = generator.generateDoc(myEvents);
      let nextIndex = myRows[myRows.length-1][1];
      let prevIndex = lastId;
      if (directionArg === 'prev') {
        prevIndex = myRows[myRows.length-1][1]+1;
        nextIndex = myRows[0][1];
      }
      const object = {
        '_links': {
          'base': `${serverName}:4000/dispositions/${req.params.disposition}/events`,
          'self': `${serverName}:4000/dispositions/${req.params.disposition}/events?` +
            `limit=${limit}&lastId=${lastId}&direction=${directionArg}`,
        },
        'EPCIS': doc,
      };
      if (((limit == myRows.length && directionArg === 'prev') || directionArg === 'next') && lastId != MAX_INT) {
        object._links.prev = `${serverName}:4000/dispositions/${req.params.disposition}/events?` +
        `limit=${limit}&lastId=${prevIndex}&direction=prev`;
      }
      if (limit == myRows.length || directionArg === 'prev') {
        object._links.next = `${serverName}:4000/dispositions/${req.params.disposition}/events?` +
            `limit=${limit}&lastId=${nextIndex}&direction=next`;
      }
      res.status(202).send(object);
    }).catch((processorError) => {
      res.status(500).send(processorError.toString());
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

//  returns epcis event with a specific dispostion and eventID
app.get('/dispositions/:disposition/events/:eventID', (req, res) => {
  dbConnect.getFieldEvent(['disposition', req.params.disposition], req.params.eventID).then((result) => {
    const myRows = result.rows;
    //  no result for query
    if (myRows.length === 0) {
      res.status(400).send('no result for disposition or eventID');
      return;
    }
    tangle.retrieveByMessageId(myRows[0][0]).then((tangleResult) => {
      //  generate single doc
      const doc = generator.singleEventDoc(tangleResult);
      res.status(202).send(doc);
    }).catch((tangleError) => {
      res.status(500).send(tangleError);
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

// Event Types

//  Returns all eventTypes wrapped in a JSON with an array
app.get('/eventTypes', (req, res) => {
  dbConnect.getFieldList('type').then((result) => {
    const object = {
      'eventTypes': [],
    };
    const myRows = result.rows;
    for (let i = 0; i < myRows.length; i++) {
      object['eventTypes'].push(myRows[i][0]);
    }
    res.status(202).send(object);
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

app.get('/eventTypes/:eventType', (req, res) => {
  res.redirect(308, '/eventTypes/' + req.params.eventType + '/events');
});

// / returns all EPCIS events with specific eventType
app.get('/eventTypes/:eventType/events', (req, res) => {
  //  pagination limits
  let limit = req.query.limit;
  let lastId = req.query.lastId;
  let direction = req.query.direction;
  let directionArg = 'next';
  if (limit == null || limit == undefined) {
    limit = DEFAULT_LIMIT;
  }
  if (lastId == null || lastId == undefined) {
    lastId = MAX_INT;
  }
  if (direction === 'prev') {
    direction = PREV;
    directionArg = 'prev';
  } else {
    direction = NEXT;
  }
  dbConnect.getFieldEvents(['type', req.params.eventType], limit, lastId, direction).then((result) => {
    console.log(result);
    const myRows = result.rows;
    // /no result for query
    if (myRows.length === 0) {
      res.status(400).send('no result for eventType');
      return;
    }
    //  generate paginated epcis doc
    process.processRows(myRows).then((myEvents) => {
      console.log(myEvents);
      const doc = generator.generateDoc(myEvents);
      let nextIndex = myRows[myRows.length-1][1];
      let prevIndex = lastId;
      if (directionArg === 'prev') {
        prevIndex = myRows[myRows.length-1][1]+1;
        nextIndex = myRows[0][1];
      }
      const object = {
        '_links': {
          'base': `${serverName}:4000/eventTypes/${req.params.eventType}/events`,
          'self': `${serverName}:4000/eventTypes/${req.params.eventType}/events?` +
            `limit=${limit}&lastId=${lastId}&direction=${directionArg}`,
        },
        'EPCIS': doc,
      };
      if (((limit == myRows.length && directionArg === 'prev') || directionArg === 'next') && lastId != MAX_INT) {
        object._links.prev = `${serverName}:4000/eventTypes/${req.params.eventType}/events?` +
          `limit=${limit}&lastId=${prevIndex}&direction=prev`;
      }
      if (limit == myRows.length) {
        object._links.next = `${serverName}:4000/eventTypes/${req.params.eventType}/events?` +
            `limit=${limit}&lastId=${nextIndex}&direction=next`;
      }
      res.status(202).send(object);
    }).catch((processorError) => {
      res.status(500).send('Tangle Record Removed');
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

//  returns epcis event with specific eventType and specific eventID in an epcis Doc
app.get('/eventTypes/:eventType/events/:eventID', (req, res) => {
  dbConnect.getFieldEvent(['type', req.params.eventType], req.params.eventID).then((result) => {
    const myRows = result.rows;
    // / no result for query
    if (myRows.length === 0) {
      res.status(400).send('no result for eventType or eventID');
      return;
    }
    tangle.retrieveByMessageId(myRows[0][0]).then((tangleResult) => {
      //  generate epcis doc
      const doc = generator.singleEventDoc(tangleResult);
      res.status(202).send(doc);
    }).catch((tangleError) => {
      res.status(500).send(tangleError);
    });
  }).catch((dbError) => {
    res.status(500).send(dbError);
  });
});

// expose listening port
const port = 4000;
app.listen(port, () => console.log('Listening on port 4000...'));
