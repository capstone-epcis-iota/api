const Ajv = require('ajv');
const addFormats = require('ajv-formats');
const schema = require('./schemas/EPCIS-JSON-Schema.json');

const ajv = new Ajv({allErrors: true, strict: false});
require('ajv-errors')(ajv, {singleError: true});
addFormats(ajv);
const validate = ajv.compile(schema);

//  export function for validating an epcis document against schema
exports.validateEpcis = function(req) {
  const result = validate(req);
  if (result) {
    return 'valid';
  } else {
    return validate.errors;
  }
};
