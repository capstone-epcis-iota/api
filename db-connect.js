// (A) LOAD DB MODULE
const {Pool} = require('pg');
const connectionInfoDB = require('./connectionConfig.js');
const SQL = require('./queries.js');
const regex = require('./regex.js');

//  SQL Database connection
const pool = new Pool({
  host: connectionInfoDB.storageConfig.host,
  user: connectionInfoDB.storageConfig.user,
  password: connectionInfoDB.storageConfig.password,
  database: connectionInfoDB.storageConfig.database,
  port: connectionInfoDB.storageConfig.port,
  max: 10, // / max of 10 clients
  keepAlive: true,
});

//  inserts a single epcis event into the epcis postgres table
exports.insertDb = async function(epcisPost, mIndex, mId) {
  //  fields to store
  const eventType = epcisPost['type'].toString();
  const messageId = mId;
  const messageIndex = mIndex;
  let bizLocation = null;
  let bizStep = null;
  let readPoint = null;
  let disposition = null;
  // check for fields in the epcis event
  if ('bizStep' in epcisPost) {
    bizStep = epcisPost['bizStep'].toString().substring(epcisPost['bizStep'].search(regex.bizStepRegex));
  }

  if ('readPoint' in epcisPost) {
    readPoint = epcisPost['readPoint'].id.toString();
  }

  if ('bizLocation' in epcisPost) {
    bizLocation = epcisPost.bizLocation.id;
  }

  if ('disposition' in epcisPost) {
    disposition = epcisPost.disposition.toString().substring(epcisPost.disposition.search(regex.dispositionRegex));
  }

  // / connect and insert into the database
  const client = await pool.connect();
  try {
    await client.query('BEGIN');
    const query = SQL.insertEPCIS(eventType, bizLocation, bizStep, readPoint,
        disposition, messageId, messageIndex);
    await client.query({
      rowMode: 'array',
      text: query,
    });
    await client.query('COMMIT');
  } catch (e) { // roll back for error
    await client.query('ROLLBACK');
    throw e;
  } finally {
    client.release();
  }
};

// will insert multiple epcs into the database into the epc_dev table
exports.insertDbEpc = async function(epcisPost, mIndex, mId) {
  //  stores epcs fields for database insert
  const epcs = [];

  //  check for fields to insert into epcs array
  if ('parentID' in epcisPost) {
    epcs.push(epcisPost['parentID'].toString());
  }

  if ('epcList' in epcisPost) {
    for (const key in epcisPost.epcList) {
      if (epcisPost.epcList.hasOwnProperty(key)) {
        epcs.push(epcisPost['epcList'][key].toString());
      }
    }
  }

  if ('childEPCs' in epcisPost) {
    for (const key in epcisPost.childEPCs) {
      if (epcisPost.childEPCs.hasOwnProperty(key)) {
        epcs.push(epcisPost['childEPCs'][key].toString());
      }
    }
  }

  if ('inputEPCList' in epcisPost) {
    for (const key in epcisPost.inputEPCList) {
      if (epcisPost.inputEPCList.hasOwnProperty(key)) {
        epcs.push(epcisPost['inputEPCList'][key].toString());
      }
    }
  }

  if ('outputEPCList' in epcisPost) {
    for (const key in epcisPost.outputEPCList) {
      if (epcisPost.outputEPCList.hasOwnProperty(key)) {
        epcs.push(epcisPost['outputEPCList'][key].toString());
      }
    }
  }

  //  insert to DB epc dev table iteratively
  const client = await pool.connect();
  try {
    for (const epc in epcs) {
      if (epcs.hasOwnProperty(epc)) {
        await client.query('BEGIN');
        await client.query({
          rowMode: 'array',
          text: `INSERT INTO public.epc_dev(epc, m_id, m_index)
                 VALUES ('${epcs[epc].toString()}', '${mId}', '${mIndex}')`,
        });
      }
    }
    //  commit the insert
    await client.query('COMMIT');
  } catch (e) { //  Rollback fails insert
    await client.query('ROLLBACK');
    throw e;
  } finally {
    client.release();
  }
};

/**
 * General function that returns data from connected database.
 * @param {string} query string containing SQL.
 * return pg.Result Shape containing returned data from database.
 */
async function getData(query) {
  const client = await pool.connect();
  try {
    return await client.query({
      rowMode: 'array',
      text: query,
    });
  } catch (e) {
    throw e;
  } finally {
    client.release();
  }
}

//  This section returns SQL Queries for each specific
//  endpoint that is available for interacting with the tangle and database
// EPC exports
exports.getEpcs = async function() {
  return getData(SQL.getEpcs());
};

exports.getEpcEvents = async function(epc, limit, lastId, direction) {
  return getData(SQL.getEpcEvents(epc, limit, lastId, direction));
};

exports.getEpcEvent = async function(epc, eventId) {
  return getData(SQL.getEpcEvent(epc, eventId));
};

// EPCIS field exports
exports.getFieldList = async function(field) {
  return getData(SQL.getFieldList(field));
};

exports.getFieldEvents = async function(field, limit, lastId, direction) {
  return getData(SQL.getFieldEvents(field, limit, lastId, direction));
};

exports.getFieldEvent = async function(field, eventId) {
  return getData(SQL.getFieldEvent(field, eventId));
};

// A function for integration tests that wipes both databases
// environment guard insures this never gets called on production
exports.wipeDB = async function() {
  if (process.env.MODE === 'TEST') {
    const client = await pool.connect();
    try {
      const query = 'DELETE FROM public.epc_dev; DELETE FROM public.epcis_events_dev;';
      await client.query({
        rowMode: 'array',
        text: query,
      });
    } catch (e) { // roll back for error
      throw e;
    } finally {
      client.release();
    }
  }
};

