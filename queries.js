// EPC queries
exports.getEpcs = function() {
  return 'SELECT DISTINCT epc FROM public.epc_dev';
};

exports.getEpcEvents = function(epc, limit, lastId, direction) {
  return `SELECT DISTINCT 
            epc.m_id,
            ee.id
          FROM 
              public.epcis_events_dev as ee,
              public.epc_dev as epc
          WHERE 
              epc.m_index = ee.m_index
              AND epc.m_id = ee.m_id
              AND epc.epc = \'${epc}\'
              AND ee.id ${direction[0]} ${lastId}
          ORDER BY id ${direction[1]}
          LIMIT ${limit}`;
};

exports.getEpcEvent = function(epc, eventId) {
  return `SELECT DISTINCT m_id
          FROM public.epc_dev
          WHERE epc = \'${epc}\'
            AND m_index = \'${eventId}\'`;
};

exports.getFieldList = function(field) {
  return `SELECT DISTINCT ${field} FROM public.epcis_events_dev 
    WHERE ${field} IS NOT NULL AND ${field} != \'null\'`;
};

exports.getFieldEvents = function(field, limit, lastId, direction) {
  return `SELECT DISTINCT
                m_id,
                id
            FROM 
                public.epcis_events_dev
            WHERE 
                ${field[0]} = \'${field[1]}\'
                AND id ${direction[0]} ${lastId}
                AND ${field[0]} IS NOT NULL 
                AND ${field[0]} != \'null\'
            ORDER BY id ${direction[1]}
            LIMIT ${limit}`;
};

exports.getFieldEvent = function(field, eventId) {
  return `SELECT DISTINCT m_id
          FROM public.epcis_events_dev
          WHERE ${field[0]} = \'${field[1]}\'
            AND m_index = \'${eventId}\'`;
};

exports.insertEPCIS = function(type, bizLocation, bizStep, readPoint, disposition, mId, mIndex) {
  const sql = 'INSERT INTO public.epcis_events_dev' +
    '(type, biz_location, biz_step, read_point, disposition, m_id, m_index)' +
    'VALUES' +
    '(\'' + type + '\',\'' + bizLocation + '\',\'' + bizStep + '\',\'' + readPoint + '\',\'' + disposition +
    '\',\'' + mId + '\',\'' + mIndex + '\')';
  return sql;
};

exports.insertEPCStatement = 'INSERT INTO public.epc_dev(epc, m_id, m_index) VALUES ';
