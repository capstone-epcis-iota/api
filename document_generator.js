// / will generate an epcis document from a passed in array

exports.singleEventDoc = function(event) {
  //  epcis document JSON object
  const epcisDocument = {
    '@context': 'https://id.gs1.org/epcis-context.jsonld', 'type': 'EPCISDocument',
    'creationDate': '',
    'schemaVersion': 2,
    'format': 'application/ld+json',
    'epcisBody': {
      'eventList': [],
    },
  };

  const fullTimestamp = createStamp();
  console.log(fullTimestamp);
  epcisDocument['creationDate'] = fullTimestamp;
  // might have to change to object later
  epcisDocument['epcisBody']['eventList'].push(JSON.parse(event));
  return epcisDocument;
};

// generates epcis document from mutiple events
exports.generateDoc = function(events) {
  const epcisDocument = {
    '@context': 'https://id.gs1.org/epcis-context.jsonld', 'type': 'EPCISDocument',
    'creationDate': '',
    'schemaVersion': 2,
    'format': 'application/ld+json',
    'epcisBody': {
      'eventList': [],
    },
  };

  // create a time stamp
  const fullTimestamp = createStamp();
  console.log(fullTimestamp);
  epcisDocument['creationDate'] = fullTimestamp;

  // push events into the epcis documents
  for (const key in events) {
    if (events.hasOwnProperty(key)) {
      const object = events[key];
      console.log(object);
      epcisDocument['epcisBody']['eventList'].push(JSON.parse(object));
    }
  }
  return epcisDocument;
};
// creates a time stamp relative to utc with a tango time offset
function createStamp() {
  const date = new Date();
  const year = date.getFullYear();
  let hour = date.getHours();
  let month = date.getMonth();
  let day = date.getDate();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();
  if (hour < 10) {
    hour = '0' + hour.toString();
  } else {
    hour = hour.toString();
  }
  if (month < 10) {
    month = '0' + month.toString();
  } else {
    month = month.toString();
  }

  if (day < 10) {
    day = '0' + day.toString();
  } else {
    day = day.toString();
  }

  if (minutes < 10) {
    minutes = '0' + minutes.toString();
  } else {
    minutes = minutes.toString();
  }

  if (seconds < 10) {
    seconds = '0' + seconds.toString();
  } else {
    seconds = seconds.toString();
  }
  // create stamp from all details
  return year + '-' + month + '-' + day + 'T' + hour + ':' + minutes + ':' + seconds + '+01:00';
}
