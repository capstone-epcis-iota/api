const tangle = require('./tangle.js');
// gets tangle epcis events and processes them and returns them into an array
exports.processRows = async function processFetch(rows) {
  // array to store events fetched from tangle
  const arr = [];
  const promises = rows.map((row) => tangle.retrieveByMessageId(row[0]));

  for (const timeoutPromise of promises) {
    const res = await timeoutPromise;
    arr.push(res);
  }
  //  when all events are retrieved return the array of events
  if (arr.length === rows.length) {
    return arr;
  }
};
